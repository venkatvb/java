import java.applet.Applet;
import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;
public class App extends Applet implements ActionListener, MouseMotionListener
{
	TextField enter;
	Button submit;
	Label list;
	String s = "";
	public void init()
	{
		setBackground(Color.red);	
	}
	public void start()
	{
		list = new Label(s);
		this.setLayout(new FlowLayout());
		enter = new TextField(50);
		submit = new Button("submit");
		this.add(enter);
		this.add(submit);
		this.add(list);
		submit.addActionListener(this);
	}
	public void paint(Graphics g)
	{
		g.drawString(getCodeBase().toString(),50, 50);
		g.draw3DRect(100, 100, 100, 100, true);
		g.fill3DRect(200, 200, 200, 200, true);
		g.drawOval(50, 80, 90, 100);
	}
	public void stop()
	{
		showStatus("stopped");
	}
	public void destroy()
	{
		showStatus("Destroyed");
	}
	public void actionPerformed(ActionEvent ae) 
	{
		showStatus("you clicked me");
		JOptionPane.showMessageDialog(this, "You clicked the button");
	}
	public void mouseDragged(MouseEvent arg0) {
		showStatus("mouse dragged");
		repaint();
	}
	public void mouseMoved(MouseEvent arg0) {
		showStatus("MOUSE MOVED");
		repaint();
	}
}