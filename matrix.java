class Matrix
{
	int r,c;
	int [][]ar;
	Matrix(int r, int c)
	{
		this.r = r;
		this.c = c;
		ar = new int[r][c];
	}
	public void getMatrix()
	{
		java.util.Scanner sc = new java.util.Scanner(System.in);
		for(int i=0;i<r;i++)
		{
			for(int j=0;j<c;j++)
			{
				ar[i][j] = sc.nextInt();
			}
		}
	}
	public Matrix multiply(Matrix b)
	{
		if(r == b.c)
		{
			Matrix c = new Matrix(this.r, b.c);
			for(int i=0;i<r;++i)
			{
				for(int j=0;j<b.c;++j)
				{
					c.ar[i][j] = 0;
					for(int k=j;k<r;k++)
					{
						c.ar[i][j] += ar[i][k]*b.ar[k][j];
					}
				}
			}
			return c;
		}
		return new Matrix(0, 0);
	}
	public String toString(Matrix c)
	{
		for(int i=0;i<c.r;i++)
		{
			for(int j=0;j<c.c;j++)
			{
				System.out.print(c.ar[i][j]+" ");
			}
			System.out.println();
		}
		return new String("");
	}
}

class Solution
{
	public static void main(String[] args) {
		Matrix a = new Matrix(2, 2),b = new Matrix(2, 2), c;
		a.getMatrix();
		b.getMatrix();
		c = a.multiply(b);
		c.toString(c);
	}
}