import java.io.*;

class StringManip
{
	public static void main(String ar[])throws Exception
	{
		System.out.println("Enter the number of strings");
		InputStreamReader ii = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(ii);
		String s = br.readLine();
		int n = Integer.parseInt(s);
		String[] name = new String[n];
		for(int i=0;i<n;i++)
		{
			name[i] = br.readLine();
		}
		for(String k:name)
		{
			System.out.println(k);
		}
	}
}