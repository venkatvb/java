package s;
import java.rmi.*;
public class sumInterfaceImpl implements sumInterface
{
	public int getSum(int a, int b)throws Exception
	{
		return a+b;
	}
	public int getSub(int a, int b)throws Exception
	{
		return a-b;
	}
	public int getMul(int a, int b)throws Exception
	{
		return a*b;
	}
	public int getDiv(int a, int b)throws Exception
	{
		return a/b;
	}
	public int getMod(int a, int b)throws Exception
	{
		return a%b;
	}
}