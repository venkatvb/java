package s;
import java.rmi.*;
public interface sumInterface extends Remote
{
	public int getSum(int a, int b)throws Exception;
	public int getMul(int a, int b)throws Exception;
	public int getSub(int a, int b)throws Exception;
	public int getDiv(int a, int b)throws Exception;
	public int getMod(int a, int b)throws Exception;
}