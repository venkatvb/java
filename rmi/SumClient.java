import java.rmi.*;
import s.*;
public class SumClient
{
	public static void main(String ds[])throws Exception
	{
		Remote ref = Naming.lookup("rmi://localhost:1099/sum");
		s.sumInterface sumref = (s.sumInterface)ref;
		int a = Integer.parseInt(ds[0]);
		int b = Integer.parseInt(ds[1]);
		int s = sumref.getSum(a, b);
		System.out.println("The Addition result is "+s);
		System.out.println("The Subtraction result is "+sumref.getSub(a,b));
		System.out.println("The Multiplication result is "+sumref.getMul(a,b));
		System.out.println("The Division result is "+sumref.getDiv(a,b));
		System.out.println("The Modulus result is "+sumref.getMod(a,b));
	}
}