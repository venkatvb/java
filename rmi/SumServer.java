import java.rmi.server.*;
import java.rmi.*;
import s.*;
class SumServer
{
	public static void main(String ds[])throws Exception
	{
		Remote remobj = new s.sumInterfaceImpl();
		UnicastRemoteObject.exportObject(remobj, 0);
		System.out.println("object exported");
		Naming.rebind("sum", remobj);
		System.out.println("object binded");
	}
}