import java.io.*;

class Palindrome
{
	public static void main(String ar[])throws Exception
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a String");
		String a = br.readLine();
		int n = a.length();
		int flag = 0;
		for(int i=0;i<n;i++)
		{
			if(a.charAt(i) != a.charAt(n-i-1))
			{
				flag = 1;
				break;
			}
		}
		if(flag == 1)
			System.out.println("Not A Palindrome");
		else
			System.out.println("The string is a Palindrome");
	}
}