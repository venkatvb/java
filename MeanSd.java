import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.Arrays;
interface Maths
{
	int mean();
	int median();
	int mode();
	int standardDeviation();
}
class Calculate implements Maths
{
	int n;
	Integer[] arr;
	Calculate(int size)
	{
		n = size;
		arr = new Integer[size];
	}
	public void get()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter "+n+" elements");
		for (int i=0; i<n; ++i) {
			arr[i] = sc.nextInt();
		}
		Arrays.sort(arr);
	}
	public int mean()
	{
		int val = 0;
		for (int i=0; i<n; ++i) {
			val += arr[i];
		}
		return val/n;
	}
	public int median()
	{
		return arr[n/2];
	}
	public int mode()
	{
		int key = arr[0], count = 0, max = -1, val = arr[0];
		for(int i=1;i<n;i++)
		{
			if(arr[i] == key)
				count++;
			else
			{
				if(max < count)
				{
					max = count;
					val = arr[i-1];
				}
				count = 0;
				key = arr[i];
			}
		}
		return val;
	}
	public int standardDeviation()
	{
		int m = mean(), sum = 0;
		for (int i=0; i<n; ++i) {
			sum += (arr[i]-m)*(arr[i]-m);
		}
		return sum/n;
	}
}
class MeanSd
{
	public static void main(String[] args) throws Exception
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of elements");
		int n = Integer.parseInt(br.readLine());
		Calculate c = new Calculate(n);
		c.get();
		System.out.println("Mean is "+c.mean());
		System.out.println("Median is "+c.median());
		System.out.println("Mode is "+c.mode());
		System.out.println("standardDeviation is "+c.standardDeviation());
	}
}