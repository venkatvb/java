class Prime
{
	public static void main(String[] args) {
		int arr[] = new int[3000000];
		arr[0] = 1;
		arr[1] = 1;
		for(int i=2;i<5000;i++)
		{
			if(arr[i] == 1)continue;
			int j=i,k=i+i;
			while(k<3000000)
			{
				arr[k] = 1;
				k += j;
			}
		}
		long sum = 0;
		for(int i=2;i<2000000;++i)
		{
			if(arr[i] == 0)
			{
				sum += (long)i;
				System.out.println(sum);
			}
		}
		System.out.println(sum);
	}
}