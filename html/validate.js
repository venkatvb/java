        function use()
        {
            var uname = document.getElementById('username').value;
            var length = uname.length;
            if(length<6)
            {
                alert("user false");
                document.getElementById('euser').style.visibility='visible';
                return false;
            }
            else
            {
                document.getElementById('euser').style.visibility='hidden';
                return true;
            }
        }
        function pas()
        {
            var pass = document.getElementById('password').value;
            var cond = false;
            for(var i=0;i<pass.length;i++)
            {
                var cc = pass.charAt(i).charCodeAt(0);
                if(cc > 47 && cc < 58)
                {
                    cond = true;
                    break;
                }
            }
            if(cond == false)
            {
                alert("passed false");
                document.getElementById('epass').style.visibility='visible';
                return false;
            }
           else
            {
                document.getElementById('epass').style.visibility='hidden';
                return true;
            }
        }
        function con()
        {
            var pass = document.getElementById('password').value;
            var con = document.getElementById('confirm').value;
            var flag = false;
            if(pass == con)
            {
                document.getElementById('econf').style.visibility='hidden';
                return true;
            }
            else
            {
                alert("confirm false");
                document.getElementById('econf').style.visibility='visible';
                return false;
            }
        }
        function ema()
        {
            var e = document.getElementById('mail').value;
            var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if(filter.test(e))
            {
                document.getElementById('eemail').style.visibility='hidden';
                return true;
            }
            else
            {
                alert("email false");
                document.getElementById('eemail').style.visibility='visible';
                return false;
            }
        }
        function all()
        {
            alert("called");
            return use() && pas() && con() && ema();
        }