function call()
{
	var quote = ["A lie told often enough becomes the truth.","All great truths begin as blasphemies.","The only difference between a rut and a grave... is in their dimensions.",
	"Writing is the only profession where no one considers you ridiculous if you earn no money.","One person with a belief is equal to a force of 99 who have only interests.",
	"Comedy is simply a funny way of being serious.","The first and great commandment is: Don't let them scare you.","Stress is an ignorant state. It believes that everything is an emergency. Nothing is that important.",
	"The wisest men follow their own direction.","True friends are those who really know you but love you anyway.","Learning to love yourself is the greatest love of all.",
	"We touch other peoples lives simply by existing."];
	var color = ["orange", "blue", "green", "yellow", "while", "grey"];
	var col = color[Math.floor((Math.random()*1000)%color.length)];
	var a = Math.floor((Math.random()*1000)%quote.length);
	var text = quote[a];
	document.getElementById("quo").innerHTML=text;
	document.getElementById("quo").style.color=col;
}