function loadXML(filename)
{
	if(window.XMLHttpRequest)
	{
		xhttp = new XMLHttpRequest();
	}
	else
	{
		xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xhttp.open("GET", filename, false);
	xhttp.send();
	return xhttp.responseXML;
}

function compare(a, b)
{
	var str1 = a.toString().trim();
	var str2 = b.toString().trim();
	return str1.localeCompare(str2)
}