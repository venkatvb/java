class Area
{
	double calculate(double a)
	{
		return 0.5*3.14*a*a;
	}
	int calculate(int a,int b)
	{
		return a*b;
	}
}
class FunctionOverload
{
	public static void main(String[] args) throws Exception
	{
		Area ar = new Area();
		java.io.BufferedReader br= new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
		System.out.println("1.Circle");	
		System.out.println("2.Rectangle");
		int n = Integer.parseInt(br.readLine());
		if(n == 1)
		{
			int a = Integer.parseInt(br.readLine());
			System.out.println(ar.calculate((double)a));
		}
		if(n == 2)
		{
			int a = Integer.parseInt(br.readLine());
			int b = Integer.parseInt(br.readLine());
			System.out.println(ar.calculate(a, b));
		}
	}
}