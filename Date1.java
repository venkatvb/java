class WrongDateException extends Exception
{
	String msg;
	WrongDateException(int n)
	{
		if(n==0)
		{
			msg = "wrong date Entered.";
		}
		if(n == 1)
		{
			msg = "wrong month Entered";
		}
		if(n == 2)
		{
			msg = "year out of range";
		}
	}
	String getError()
	{
		return msg;
	}
}
class Date 
{
	int dd, mm, yy;
	public static boolean isLeap(int year)
	{
		if(year%400 == 0)return true;
		if(year%100 == 0)return false;
		if(year%4 == 0)return true;
		return false;
	}
	Date(int dd, int mm, int yy)throws WrongDateException
	{
		this.dd = dd;
		this.mm = mm;
		this.yy = yy;
		if(yy<=1000 || yy>2200)throw new WrongDateException(2);
		if(mm<=0||mm>12)throw new WrongDateException(1);
		int val[] = {31,28,31,30,31,30,31,31,30,31,30,31};
		if(isLeap(yy))val[1]++;
		if(dd<=0||dd>val[mm-1])throw new WrongDateException(0);
	}
}

class Date1
{
	public static void main(String[] args) throws Exception
	{
		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Enter dd mm yy");
		int dd = sc.nextInt();
		int mm = sc.nextInt();
		int yy = sc.nextInt();
		try
		{
			Date d = new Date(dd,mm,yy);
			System.out.println("correct date");
		}
		catch(WrongDateException e)
		{
			System.out.println("Error "+e.getError());
		}
	}
}