class First
{
	First()
	{
		System.out.println("This is class First");
	}
}

class Second extends First
{
	Second()
	{
		super();
		System.out.println("This is class Second");
	}
}

class Th
{
	public static void main(String[] args) {
		Second c = new Second();
	}
}