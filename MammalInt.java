package animals;
public class MammalInt implements Animal
{
	public void eat()
	{
		System.out.println("Mammals 
			eat");
	}
	public int legs()
	{
		return 2;
	}
	public static void main(String[] args) 
	{
		MammalInt m = new MammalInt();
		m.eat();
		System.out.println("Number of legs is "+m.legs());	
	}
}