interface Quotation
{
	void display_item();
}
interface Payout
{
	int pays(int amount);
}
class Invoic implements Quotation, Payout
{
	private int cost[], noOfItems, totalCost, items[];
	private String name[];
	Invoic(int n)
	{
		noOfItems = n;
		name = new String[noOfItems];
		cost = new int[noOfItems];
		items = new int[noOfItems];
		totalCost = 0;
	}
	void getCost()
	{
		try
		{
			System.out.println("Enter the name and cost and no. of items of "+noOfItems+" items");
			java.io.BufferedReader br = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
			for(int i=0;i<noOfItems;++i)
			{
				System.out.println("Enter "+(i+1)+" item");
				name[i] = br.readLine();
				cost[i] = Integer.parseInt(br.readLine());
				items[i] = Integer.parseInt(br.readLine());
				totalCost += cost[i]*items[i];
			}
		}
		catch(Exception e)
		{
			System.out.println("Wrong input format Entered "+e);
		}

	}
	public void display_item()
	{
		System.out.println("Name\tCost\tNo.\tcost");
		for (int i=0; i<noOfItems; ++i) 
		{
			System.out.print(name[i]+"\t");
			System.out.print(cost[i]+"\t");
			System.out.print(items[i]+"\t");
			System.out.println(cost[i]*items[i]);	
		}
		System.out.println("The total amount to be paid is "+totalCost);
	}
	public int pays(int amount)
	{
		if(amount < totalCost)
		{
			System.out.println("Insufficient amount paid. Enter valid amount");
			java.util.Scanner sc = new java.util.Scanner(System.in);
			int n = sc.nextInt();
			return(pays(n));
		}
		return amount - totalCost;
	}
}

class Invoice
{
	public static void main(String[] args) throws Exception

	{
		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Enter the number of items");
		int n = sc.nextInt();
		Invoic i = new Invoic(n);
		i.getCost();
		i.display_item();
		System.out.println("Enter the amount the customer pays");
		n = sc.nextInt();
		System.out.println("Thank you for shopping with us your balance is "+i.pays(n));
	}
}