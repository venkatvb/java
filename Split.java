import java.io.*;
class Split
{
	public static void main(String aar[])throws Exception
	{
		try
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Enter 10 elements");
			String s = br.readLine();
			String []ar = s.split(" ");
			for(String i:ar)
				System.out.println(i);	
			int n = ar.length;
			System.out.println("The length of array is "+n);
			int []arr = new int[n];
			int pos = 0;
			for(String i:ar)
				arr[pos++] = Integer.parseInt(i);
			pos = 0;
			for(int i:arr)
				pos += i;
			System.out.println("Sum of the array is.."+pos);
		}
		catch(Exception e)
		{
			System.out.println("you got an Exception");
		}
	}
}