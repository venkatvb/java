class Product
{
	int pid;
	Product(int i)
	{
		pid = i;
	}
	public void display()
	{
		System.out.println("product id is "+pid);
	}
}
class Book extends Product
{
	String bname;
	Book(int i, String s)
	{
		super(i);
		bname = s;
	}
	public void display()
	{
		System.out.println("Product id is "+pid);
		System.out.println("The book  name is "+bname);
	}	
}
class Cd extends Product
{
	String cname;
	Cd(int i, String s)
	{
		super(i);
		cname = s;
	}
	public void display()
	{
		System.out.println("Product id is "+pid);
		System.out.println("the Cd name is "+cname);
	}
}
class Scientific extends Book
{
	String subject;
	Scientific(int i, String a, String b)
	{
		super(i, a);
		subject = b;
	}
	public void display()
	{
		System.out.println("Product id is "+pid);
		System.out.println("The book  name is "+bname);
		System.out.println("The subject is "+subject);
	}
}

class Solution
{
	public static void main(String[] args) 
	{
		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Enter the product id");
		Product p = new Product(sc.nextInt());
		System.out.println("Enter book id and book name");
		Book b = new Book(sc.nextInt(), sc.next());	
		System.out.println("Enter cd id and cd name");
		Cd c = new Cd(sc.nextInt(), sc.next());
		System.out.println("Enter subject,  pid and name of Scientific book");
		Scientific s = new Scientific(sc.nextInt(), sc.next(), sc.next());
		System.out.println();
		System.out.println();
		p.display();
		p=b;
		System.out.println();
		System.out.println();
		p.display();
		p=c;
		System.out.println();
		System.out.println();
		p.display();
		p=s;
		System.out.println();
		System.out.println();
		p.display();
	}
}