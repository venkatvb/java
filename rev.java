import java.util.*;
class Solution {
    static public int reverse(int x) {
		int f = 0, flag = -1;
		if(x < 0){flag=1;f=1;}
		Integer val = new Integer(x);
		String v = val.toString();
		String r = "";
		for(;f<v.length();++f)       
			r = v.charAt(f)+r;
		return Integer.parseInt(r)*flag*(-1);
    }
    public static void main(String dfs[])
    {
    	Scanner sc = new Scanner(System.in);
    	while(true)
    	{
    		int x = sc.nextInt();
    		System.out.println(reverse(x));
    	}
    }
}