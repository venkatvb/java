class Link
{
	int data;
	Link next;
	Link(int d, Link n)
	{
		data = d;
		next = n;
	}
}
class Stack
{
	private Link l;
	Stack()
	{
		l=null;
	}
	public boolean push(int a)
	{
		l = new Link(a, l);
		return true;
	}
	public boolean  pop()
	{
		if(l==null)return false;
		l=l.next;
		return true;
	}
	public int top()
	{
		if(l==null)return -1;
		return l.data;
	}
}
class Solution
{
	public static void main(String[] args) {
		Stack st = new Stack();
		String s;
		java.util.Scanner sc = new java.util.Scanner(System.in);
		while(true)
		{
			s = sc.next();
			if(s.equals("exit"))break;
			if(s.equals("push"))
			{
				int a = sc.nextInt();
				st.push(a);
			}
			if(s.equals("pop"))
			{
				st.pop();
			}
			if(s.equals("top"))
			{
				System.out.println(st.top());
			}

		}
	}
}