import java.io.DataInputStream;
import java.io.InputStream;
import java.net.Socket;
import java.io.OutputStream;
import java.io.DataOutputStream;
class MyClient
{
	void start()
	{
		try
		{
			Socket sc = new Socket("127.0.0.1",1244);
			OutputStream out = sc.getOutputStream();
			DataOutputStream dos = new DataOutputStream(out);
			dos.writeUTF("Hello from server");
			InputStream in = sc.getInputStream();
			DataInputStream dis = new DataInputStream(in);
			System.out.println("This is the message from the server "+dis.readUTF());
			sc.close();
		}
		catch(Exception e)
		{
			System.out.println();
		}
	}
}
class client
{
	public static void main(String ds[])
	{
		MyClient m = new MyClient();
		m.start();
	}
}