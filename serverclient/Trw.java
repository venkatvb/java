import java.io.*;
class Trw extends Thread
{
    boolean check = true, read = true;
	java.net.Socket here;
	public Trw(java.net.Socket s)
	{
		try
		{
			here = s;
			Thread t1 = new Thread(this,"reading");
			t1.start();
			while(read);
			Thread t2 = new Thread(this,"writing");
			t2.start();
		}
		catch(Exception e)
		{
			System.out.println("Interrupted Exception");
		}
	}
	public void run()	
	{
		if(read)
		{
			read = false;
			try
			{
				while(check)
				{
					DataInputStream dis = new DataInputStream(here.getInputStream());
					System.out.println(dis.readUTF());
				}
			}
			catch(Exception e)
			{
				System.out.println("Reading Exception");
			}
		}
		else
		{
			java.util.Scanner sc = new java.util.Scanner(System.in);
			try
			{
				while(check)
				{
					String text = sc.next();
					if(text.equals("exit"))break;
					DataOutputStream dos = new DataOutputStream(here.getOutputStream());
					dos.writeUTF(text);
				}
			}
			catch(Exception e)
			{
				System.out.println("writing Exception");
			}
		}
	}
}