import java.util.Vector;
import java.util.Enumeration;
import java.util.Collections;
class Is
{
	int isPalin(String s)
	{
		StringBuffer sb =  new StringBuffer(s);
		sb.reverse();
		if(s.equals(sb.toString()))
			return 1;
		return 0;
	}
}

class Palin
{
	public static void main(String[] args) {
		Vector v = new Vector();
		for(int i=900;i<=999;i++)
		{
			for (int j=i+1;j<=999 ;++j ) {
				v.addElement(String.valueOf(i*j));
			}
		}
		Collections.sort(v, Collections.reverseOrder());
		Enumeration e = v.elements();
		Is i = new Is();
		while(e.hasMoreElements())
		{
			String s =(String) e.nextElement();
			if(i.isPalin(s) == 1)
			{
				System.out.println(s);
				break;
			}
		}
	}
}