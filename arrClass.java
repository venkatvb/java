class A
{
	A()
	{
		System.out.println("A constructor");
	}
	void disp()
	{
		System.out.println("this is class A");
	}
}
class B extends A
{
	B()
	{
		super();
		System.out.println("B constructor");
	}
	void disp()
	{
		System.out.println("This is class B");
	}
}
class C extends B
{
	C()
	{
		super();
		System.out.println("C constructor");
	}
	void disp()
	{
		System.out.println("This is class c");
	}
}
class arrClass
{
	public static void main(String[] args) 	
	{
		C ar[] = new C[10];
		System.out.println(ar.length);	
		ar[0] = new C();
		ar[0].disp();
	}
}