class A
{
	A()
	{
		System.out.println("const a");
	}
	A(int n)
	{
		System.out.println("Pamametrized of class A");
	}
	int a;
	void disp()
	{

	}
}
class B extends A
{

	int b;
	B()
	{
		super(100);
		System.out.println("this is class B");
		System.out.println("const b");
	}
}

class M
{
	public static void main(String[] args) 	
	{
		B b = new B();
	}
}