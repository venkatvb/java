class A
{
	void message()
	{
		System.out.println("printing A");
	}
}
class B extends A
{
	void message()
	{
		super.message();
		System.out.println("printing B");
	}
}

class C
{
	
	public static void main(String[] args) {
		B one= new B();
		one.message();
	}
}
