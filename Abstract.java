abstract class Sample
{
	abstract void display();
}
class UseSample extends Sample
{
	void display()
	{
		System.out.println("this is display  method in UseSample");
	}
	protected void finalize()
	{
		System.out.println("this is the finalize method");
	}
}
class Abstract
{
	public static void main(String[] args) {
		Sample s = new UseSample();
		s.display();
	}
}