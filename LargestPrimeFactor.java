import java.util.Scanner;
class LargestPrimeFactor
{
	long n;
	LargestPrimeFactor(long no)
	{
		n = no;
	}
	
		long prime(long a, long i)
		{
			try
			{
				System.out.println("The a value is "+a);
				while(a%i != 0 && i*i<n)
					i = i+1;
				System.out.println("The i value is "+i);
				if(i<n)
					return prime((n/i),i);
				else
					return n;
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
			return -1;
		}
}
class Prime
{
	public static void main(String[] args)throws Exception 
	{
		Scanner sc = new Scanner(System.in);
		Long n = sc.nextLong();
		LargestPrimeFactor l = new LargestPrimeFactor(n);
		System.out.println(l.prime(n, 2));
	}
}