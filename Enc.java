class Encrypt
{
	String cipher;
	Encrypt(String a)
	{
		cipher = "";
		for(int i=1;i<a.length();i+=2)
		{
			cipher += (char)(a.charAt(i)+i);
		}
		for(int i=0;i<a.length();i+=2)
		{
			cipher += (char)(a.charAt(i)+i);
		}
	}
	public String getCipher()
	{
		return cipher;
	}
}
class Decrypt
{
	char []st;
	String normalText;
	Decrypt(String s)
	{
		normalText = "";
		st = new char[s.length()];
		int pos = 0;
		for(int i=1;i<s.length();i+=2)
		{
			st[i] = (char)(s.charAt(pos)-i);
			pos++;
		}
		for(int i=0;i<s.length();i+=2)
		{
			st[i] = (char)(s.charAt(pos)-i);
			pos++;
		}
		for(int i=0;i<st.length;++i)
		{
			normalText+=st[i];
		}
	}
	public String getNormalText()
	{
		return normalText;
	}
}

class Enc
{
	public static void main(String[] args)throws Exception 
	{
		System.out.println("Enter a String to be Encrypted");
		String s = new java.io.BufferedReader(new java.io.InputStreamReader(System.in)).readLine();
		Encrypt e = new Encrypt(s);	
		String cipher = e.getCipher();
		System.out.println("The cipher text is :"+cipher);
		Decrypt d = new Decrypt(cipher);
		System.out.println("The normalText is :"+d.getNormalText());
	}
}