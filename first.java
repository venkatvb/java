import java.io.*;

class First
{
	public static void main(String ar[])throws Exception
	{
		System.out.println("Hello world.");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of employees.");
		int n = Integer.parseInt(br.readLine());
		int number[] = new int[n];
		String name[] = new String[n];
		for(int i=0;i<n;i++)
		{
			System.out.println("Enter employee number.");
			number[i] = Integer.parseInt(br.readLine());
			System.out.println("Enter employee name");
			name[i] = br.readLine();
		}
		System.out.println("Employee details..");
		for(int i=0;i<n;i++)
		{
			System.out.println(number[i] + "   " + name[i]);
		}
	}
}